package nz.michaelrausch.modules;

import sx.blah.discord.handle.obj.IMessage;

public abstract class Module {
    public abstract void onCommand(String command, String args, IMessage message);
}
