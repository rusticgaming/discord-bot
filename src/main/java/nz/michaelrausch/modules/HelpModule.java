package nz.michaelrausch.modules;

import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.EmbedBuilder;

import java.awt.*;

public class HelpModule extends Module {

    @Override
    public void onCommand(String command, String args, IMessage message) {
        EmbedBuilder builder = new EmbedBuilder();

        builder.appendField("/apply", "Apply to become a staff member", false);
        builder.appendField("/contact <name>", "Contact a staff member", false);
        builder.appendField("/staff", "Get a list of all staff members", false);

        builder.withTitle("Help");
        builder.withColor(Color.GREEN);

        message.reply("", builder.build());
    }
}
