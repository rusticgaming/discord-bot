package nz.michaelrausch.modules;

import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.EmbedBuilder;

import java.awt.*;

public class ApplyModule extends Module {
    @Override
    public void onCommand(String command, String args, IMessage message) {
        EmbedBuilder builder = new EmbedBuilder();

        builder.appendField("Become a staff member", "https://app.rusticgaming.com :fire:", false);

        builder.withTitle("Apply");
        builder.withColor(Color.CYAN);

        message.reply("", builder.build());
    }
}
