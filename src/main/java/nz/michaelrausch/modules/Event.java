package nz.michaelrausch.modules;

import java.util.ArrayList;
import java.util.List;

public class Event {
    private final String time;
    private final String name;
    private List<String> players;

    public Event(String name, String time){
        this.players = new ArrayList<>();
        this.name = name;
        this.time = time;
    }

    public void addPlayer(String playerName){
        this.players.add(playerName);
    }

    public String getName() {
        return name;
    }

    public String getTime() {
        return time;
    }

    public List<String> getPlayers() {
        return players;
    }
}
