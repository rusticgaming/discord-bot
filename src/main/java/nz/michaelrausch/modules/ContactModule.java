package nz.michaelrausch.modules;

import nz.michaelrausch.builders.ContactMessageBuilder;
import sx.blah.discord.handle.obj.IMessage;

public class ContactModule extends Module {

    @Override
    public void onCommand(String command, String args, IMessage message) {
        ContactMessageBuilder builder = new ContactMessageBuilder();

        if (args.toUpperCase().equals("TIM")){
            builder.withName("Tim Mangos")
                    .withEmail("tcmangos@timmangos.com")
                    .withIcon("https://pbs.twimg.com/profile_images/634992583261814785/wfh0n3G6_400x400.jpg");
        }

        else if (args.toUpperCase().equals("MICHAEL")){
            builder.withName("Michael Rausch ")
                    .withEmail("rustic@michaelrausch.nz")
                    .withIcon("https://i.imgur.com/9pZdmmn.png")
                    .withWebsite("http://michaelrausch.nz")
                    .withPhone("(+64) 27-952-9554");
        }

        else {
            builder.withName("There are no staff members with that name (╯°□°）╯︵ ┻━┻");

            builder.getEmbedBuilder().appendField("Here are staff members that have contact information set up", "tim, michael", false);
            builder.getEmbedBuilder().withImage("https://thumbs.gfycat.com/RequiredExcellentDarwinsfox-max-1mb.gif");
        }

        message.reply("", builder.build());
    }
}
