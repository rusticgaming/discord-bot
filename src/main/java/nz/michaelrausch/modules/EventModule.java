package nz.michaelrausch.modules;

import nz.michaelrausch.BotMain;
import sx.blah.discord.handle.obj.IGuild;
import sx.blah.discord.handle.obj.IMessage;

import java.util.ArrayList;
import java.util.List;

public class EventModule extends Module {
    private List<Event> events;

    public EventModule(){
        super();

        events = new ArrayList<>();
    }

    @Override
    public void onCommand(String command, String args, IMessage message) {
        switch (command){
            case "event":
                newEvent(args, message);
                break;

            case "attend":
                attendEvent(args, message);
                break;

            case "events":
                listEvents(args, message);

            default:
                break;
        }
    }

    private void attendEvent(String args, IMessage message) {
        for (Event e : events){
            if (e.getName().toUpperCase().equals(args.toUpperCase())){
                e.addPlayer(message.getAuthor().getName());
                message.reply("You are attending");
                return;
            }
        }

        message.reply("Couldn't find that event");
    }

    private void newEvent(String args, IMessage message) {
        String[] argList = args.split(" ");

        if (argList.length < 2){
            message.reply("!event <name> <time>");
            return;
        }

        String time = argList[argList.length - 1];
        Integer timePos = args.indexOf(time);

        String name = args.substring(0, timePos - 1);

        Event e = new Event(name, time);

        events.add(e);

        message.reply("Created! :poop:");
    }

    private void listEvents(String args, IMessage message){
        String eventList = "";

        for (Event e : events){
            eventList += e.getName() + " @ " + e.getTime() + ", " + e.getPlayers().size() + " players";
        }

        message.reply(eventList);
    }
}
