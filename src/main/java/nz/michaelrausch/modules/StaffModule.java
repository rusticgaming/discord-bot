package nz.michaelrausch.modules;

import sx.blah.discord.handle.obj.IMessage;
import sx.blah.discord.util.EmbedBuilder;

import java.awt.*;

public class StaffModule extends Module {

    @Override
    public void onCommand(String command, String args, IMessage message) {
        EmbedBuilder builder = new EmbedBuilder();

        builder.withTitle("Rustic Gaming Staff Members");
        builder.appendField("Owner", "Tim#4979\n", false);
        builder.appendField("Assistant", "taylorkeay1#7507", false);
        builder.appendField("Manager", "Meliodas#0001", false);
        builder.appendField("Lead Developer", "michael#0374", false);
        builder.appendField("Developers", "DizMizzer#9574\nGunmar#4320\nTree#0576", false);
        builder.appendField("Senior Administrator", "Dilskillz#5739", false);
        builder.appendField("Administrators", "MonkeysRcool#3298\nHogwarts1911#9588", false);
        builder.appendField("Moderator", "EquinoxRift#6795", false);
        builder.appendField("Head Builder", "YellowSage#2958", false);
        builder.appendField("General Builders", "CrazychickenYT#0952\nGunmar#4320\nWaterski#7377\niTzChloeySenpai#0020\nitzZaeSenpai#2443", false);

        builder.withColor(Color.ORANGE);

        message.reply("", builder.build());
    }
}
