package nz.michaelrausch.builders;

import sx.blah.discord.api.internal.json.objects.EmbedObject;

import java.awt.*;

public class ContactMessageBuilder extends MessageBuilder{
    public ContactMessageBuilder(){
        super();
        getEmbedBuilder().withColor(Color.green);
    }

    public ContactMessageBuilder withName(String name){
        getEmbedBuilder().withAuthorName(name);
        return this;
    }

    public ContactMessageBuilder withEmail(String email){
        getEmbedBuilder().appendField("Email", email, false);
        return this;
    }

    public ContactMessageBuilder withIcon(String iconUrl){
        getEmbedBuilder().withThumbnail(iconUrl);
        return this;
    }

    public ContactMessageBuilder withWebsite(String url){
        getEmbedBuilder().withAuthorUrl(url);
        getEmbedBuilder().appendField("Website", url, false);
        return this;
    }

    @Override
    public EmbedObject build() {
        return getEmbedBuilder().build();
    }

    public ContactMessageBuilder withPhone(String phone) {
        getEmbedBuilder().appendField("Phone", phone, false);
        return this;
    }

}
