package nz.michaelrausch.builders;

import sx.blah.discord.api.internal.json.objects.EmbedObject;
import sx.blah.discord.util.EmbedBuilder;

public abstract class MessageBuilder {
    private EmbedBuilder builder;

    public MessageBuilder(){
        builder = new EmbedBuilder();
    }

    public EmbedBuilder getEmbedBuilder(){
        return builder;
    }

    abstract EmbedObject build();
}
