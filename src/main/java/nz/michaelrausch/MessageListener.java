package nz.michaelrausch;

import nz.michaelrausch.modules.Module;
import sx.blah.discord.api.events.IListener;
import sx.blah.discord.handle.impl.events.guild.channel.message.MessageEvent;

import java.util.HashMap;
import java.util.Map;

public class MessageListener implements IListener<MessageEvent>{
    private String PREFIX = "!";
    private Map<String, Module> commands;

    public MessageListener(){
        commands = new HashMap<>();
    }

    @Override
    public void handle(MessageEvent event) {
        if (!event.getMessage().getContent().startsWith(PREFIX)) return;

        String message = event.getMessage().getContent().substring(1);
        String command;
        String args = "";

        Integer commandEndIndex = message.indexOf(" ");

        if (commandEndIndex > 0){
            command = message.substring(0, commandEndIndex);
            args = message.substring(commandEndIndex + 1);
        }
        else{
            command = message;
        }

        if (!commands.keySet().contains(command)) {
            event.getMessage().reply("Sorry, that's not something i know how to do :(");
            return;
        }

        Module handler = commands.get(command);
        handler.onCommand(command, args, event.getMessage());
    }

    public void registerCommand(String cmd, Module module){
        commands.put(cmd, module);
    }
}
