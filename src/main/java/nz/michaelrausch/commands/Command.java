package nz.michaelrausch.commands;

import sx.blah.discord.handle.obj.IMessage;

public abstract class Command {
    public abstract void onCommand(String command, String args, IMessage message);
}
