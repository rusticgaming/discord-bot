package nz.michaelrausch.commands;

import sx.blah.discord.handle.obj.IMessage;

public class ContactCommand extends Command {

    @Override
    public void onCommand(String command, String args, IMessage message) {
        if (args.toUpperCase().equals("TIM")) message.reply("You can contact tim at: tcmangos@timmangos.com");
        if (args.toUpperCase().equals("MICHAEL")) message.reply("You can contact michael at: http://michaelrausch.nz/contact");
    }
}
