package nz.michaelrausch.commands;

import sx.blah.discord.handle.obj.IMessage;

public class HelpCommand extends Command{

    @Override
    public void onCommand(String command, String args, IMessage message) {
        message.reply("Commands: /apply, /help, /contact <name>");
    }
}
