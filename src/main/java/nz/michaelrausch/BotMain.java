package nz.michaelrausch;

import nz.michaelrausch.modules.*;
import nz.michaelrausch.modules.ApplyModule;
import nz.michaelrausch.modules.HelpModule;
import sx.blah.discord.api.ClientBuilder;
import sx.blah.discord.api.IDiscordClient;
import sx.blah.discord.api.events.EventDispatcher;
import sx.blah.discord.util.DiscordException;

public class BotMain {
    private static String DISCORD_TOKEN = "";
    public static IDiscordClient client;

    private static IDiscordClient buildClient(String tok, boolean login){
        ClientBuilder builder = new ClientBuilder();
        builder.withToken(tok);

        try{
            if (login) return builder.login();
            else return builder.build();
        }
        catch (DiscordException e){
            e.printStackTrace();
            return null;
        }
    }

    public static void main(String[] args){
        client = buildClient(DISCORD_TOKEN, true);
        MessageListener listener = new MessageListener();

        if (client == null) return;

        EventDispatcher dispatcher = client.getDispatcher();

        EventModule eventModule = new EventModule();

        listener.registerCommand("help", new HelpModule());
        listener.registerCommand("contact", new ContactModule());
        listener.registerCommand("apply", new ApplyModule());
        listener.registerCommand("staff", new StaffModule());

        listener.registerCommand("event", eventModule);
        listener.registerCommand("attend", eventModule);
        listener.registerCommand("events", eventModule);

        dispatcher.registerListener(listener);
    }
}
